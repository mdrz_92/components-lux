import React from 'react'
import { Switch, Route } from 'react-router-dom'

// Private Routes
import {PrivateRoute} from './components/PrivateRoute'

// Pages components
import Test from './pages/test'

const Main = () => (
  <main className="main">
    <Switch>
      <Route exact path='/' component={Test} />
      <Route exact path='/goal' component={Test} />
      <PrivateRoute exact path='/profile' component={Test} />
    </Switch>
  </main>
)

export default Main