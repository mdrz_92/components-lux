/*
 src/index.js
*/
import React from 'react';
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { BrowserRouter } from 'react-router-dom'

import configureStore from './store';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

import './index.css';
import 'semantic-ui-css/semantic.min.css'

render(
 <Provider store={configureStore()}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
 </Provider>,
 document.getElementById('root')
);

registerServiceWorker();
