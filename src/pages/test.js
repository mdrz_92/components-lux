import React, { Component } from 'react';
import { connect } from 'react-redux';
import ButtonLux from '../components/basics/Button';
import ContainerLux from '../components/basics/Container';
import '../App.css';
import { simpleAction } from '../actions/simpleAction';
import { Icon } from 'semantic-ui-react';
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      content  : null, 
    };
  }

  componentDidMount() {
    this.setState({content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pulvinar iaculis leo, eu vulputate ipsum volutpat sed. Cras sodales ex porttitor interdum suscipit. Phasellus suscipit lectus eu malesuada blandit. Nulla egestas eros vitae tempus convallis. Aliquam erat volutpat. Duis vel imperdiet tellus. Mauris molestie, nibh ac aliquet faucibus, elit sem aliquam nunc, a tristique lectus mi at ex. Curabitur eleifend ornare tortor, nec suscipit tellus volutpat et. Aenean egestas, enim quis consectetur vestibulum, sem orci varius nunc, vitae rutrum mi urna at leo. In finibus dui at maximus posuere. Nullam consequat ligula sapien, sit amet iaculis turpis hendrerit et. Pellentesque a dolor metus. Vivamus purus leo, convallis sit amet gravida ac, suscipit nec lectus. Proin commodo dignissim velit, id lobortis orci pharetra et."})
  }

  simpleAction = (event) => {
    this.props.simpleAction();
  }

  render() {
    return (
    <div className="App">
      <ContainerLux textAlign='justified' content={this.state.content} contentHeader={'Title Example'} sizeHeader="huge" iconHeader textAlignHeader="center" subheaderHeader="remark this is a example element" iconContentHeader={<Icon name='home' size='large' />}/>
      <ButtonLux icon={[<Icon name="shop" />]} onClick={this.simpleAction} circular content ={'ClickMe'}/>
    </div>
    );
 }
}


const mapStateToProps = state => ({
  ...state
 })

 const mapDispatchToProps = dispatch => ({
  simpleAction: () => dispatch(simpleAction())
 })


export default connect(mapStateToProps, mapDispatchToProps)(App);
