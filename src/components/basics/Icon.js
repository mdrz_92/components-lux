import React, { Component } from 'react';
import { Icon } from 'semantic-ui-react'
class IconLux extends Component {
     render() {
      return (
        <div>
            <Icon
              className={this.props.className && this.props.className}
              aria-hidden={this.props.ariaHidden && this.props.ariaHidden}
              aria-label={this.props.ariaLabel && this.props.ariaLabel}
              bordered={this.props.bordered && this.props.bordered}
              circular={this.props.circular && this.props.circular}
              color={this.props.color && this.props.color}
              corner={this.props.corner && this.props.corner}
              disabled={this.props.disabled && this.props.disabled}
              fitted={this.props.fitted && this.props.fitted}
              flipped={this.props.flipped && this.props.flipped}
              inverted={this.props.inverted && this.props.inverted}
              link={this.props.link && this.props.link}
              loading={this.props.loading && this.props.loading}
              name={this.props.name && this.props.name}
              rotated={this.props.rotated && this.props.rotated}
              size={this.props.size && this.props.size}
            >
            </Icon>
        </div>
      );
   }
  }

export default IconLux