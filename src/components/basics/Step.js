import React, { Component } from 'react';
import { Step } from 'semantic-ui-react'

class StepLux extends Component {
     render() {
      return (
        <div>
          <Step 
            attached={this.props.attached && this.props.attached}
            children={this.props.children && this.props.children}
            className={this.props.className && this.props.className}
            fluid={this.props.fluid && this.props.fluid}
            items={this.props.items && this.props.items}
            ordered={this.props.ordered && this.props.ordered}
            size={this.props.size && this.props.size}
            stackable={this.props.stackable && this.props.stackable}
            unstackable={this.props.unstackable && this.props.unstackable}
            vertical={this.props.vertical && this.props.vertical}
            widths={this.props.widths && this.props.widths}
          >
          {this.props.stepTitle && <Step.Title
            children={this.props.childrenTitle && this.props.childrenTitle}
            className={this.props.classNameTitle && this.props.classNameTitle}
            content={this.props.contentTitle && this.props.contentTitle}
          >
          </Step.Title>}
          {this.props.stepDescription && <Step.Description
            children={this.props.childrenDescription && this.props.childrenDescription}
            className={this.props.classNameDescription && this.props.classNameDescription}
            content={this.props.contentDescription && this.props.contentDescription}
          >
          </Step.Description>}
        </Step>
        </div>
      );
   }
  }

export default StepLux