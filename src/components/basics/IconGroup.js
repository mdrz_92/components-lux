import React, { Component } from 'react';
import { Icon } from 'semantic-ui-react'
class IconGroupLux extends Component {
     render() {
      return (
        <div>
            <Icon.Group
              className={this.props.className && this.props.className}
              size={this.props.size && this.props.size}
              content={this.props.content && this.props.content}
            >
                {this.props.icons && this.props.icons}
            </Icon.Group>
        </div>
      );
   }
  }

export default IconGroupLux