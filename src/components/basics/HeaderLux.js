import React, { Component } from 'react';
import { Header } from 'semantic-ui-react'
class HeaderLux extends Component {
     render() {
      return (
        <div>
            <Header
              attached={this.props.attached ? this.props.attached : false}
              block={this.props.block ? this.props.block : false}
              className={this.props.className && this.props.className}
              color={this.props.color && this.props.color}
              size={this.props.size && this.props.size}
              subheader={this.props.subheader && this.props.subheader}
              textAlign={this.props.textAlign && this.props.textAlign}
              icon= {this.props.icon && this.props.icon}
            >
            {this.props.content && this.props.content}
            {this.props.subheader && <Header.Subheader>
              {this.props.subheader}
            </Header.Subheader>
            }
            {this.props.iconContent && this.props.iconContent}
            </Header>
        </div>
      );
   }
  }

export default HeaderLux