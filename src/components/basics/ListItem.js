import React, { Component } from 'react';
import { List } from 'semantic-ui-react'

class ListItemLux extends Component {
     render() {
      return (
        <div>
          <List.Item 
            active={this.props.active && this.props.active}
            children={this.props.children && this.props.children}
            className={this.props.className && this.props.className}
            content={this.props.content && this.props.content}
            description={this.props.description && this.props.description}
            disabled={this.props.disabled && this.props.disabled}
            header={this.props.header && this.props.header}
            icon={this.props.icon && this.props.icon}
            image={this.props.image && this.props.image}
            onClick={this.props.onClick && this.props.onClick}
            value={this.props.value && this.props.value}
          >
            {this.props.itemIcon && <List.Icon
                className={this.props.classNameIcon && this.props.classNameIcon}
                verticalAlign={this.props.verticalAlignIcon && this.props.verticalAlignIcon}
            >
            </List.Icon>
            }
            {this.props.itemContent && <List.Content
                children={this.props.childrenContent && this.props.childrenContent}
                className={this.props.classNameContent && this.props.classNameContent}
                content={this.props.contentContent && this.props.contentContent}
                description={this.props.descriptionContent && this.props.descriptionContent}
            >
                {this.props.itemIcon && <List.Header
                children={this.props.childrenHeader && this.props.childrenHeader}
                className={this.props.classNameHeader && this.props.classNameHeader}
                content={this.props.contentHeader && this.props.contentHeader}
                >
                </List.Header>
                }
                {this.props.itemIcon && <List.Description
                children={this.props.childrenDescription && this.props.childrenDescription}
                className={this.props.classNameDescription && this.props.classNameDescription}
                content={this.props.contentDescription && this.props.contentDescription}
                >
                </List.Description>
                }
                {this.props.itemIcon && <List.List
                children={this.props.childrenList && this.props.childrenList}
                className={this.props.classNameList && this.props.classNameList}
                content={this.props.contentList && this.props.contentList}
                >
                </List.List>
                }
            </List.Content>
            }
        </List.Item>
        </div>
      );
   }
  }

export default ListItemLux