import React, { Component } from 'react';
import { Image } from 'semantic-ui-react'

class ImageGroupLux extends Component {
     render() {
      return (
        <div>
          <Image.Group
            className={this.props.className && this.props.className}
            content={this.props.content && this.props.content}
            size={this.props.size && this.props.size}
          >
            {this.props.images && this.props.images}
          </Image.Group>
        </div>
      );
   }
  }

export default ImageGroupLux