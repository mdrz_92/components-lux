import React, { Component } from 'react';
import { Image } from 'semantic-ui-react'

class ImageLux extends Component {
     render() {
      return (
        <div>
          <Image 
            avatar={this.props.avatar && this.props.avatar}
            bordered={this.props.bordered && this.props.bordered}
            centered={this.props.centered && this.props.centered}
            circular={this.props.circular && this.props.circular}
            className={this.props.className && this.props.className}
            content={this.props.content && this.props.content}
            dimmer={this.props.dimmer && this.props.dimmer}
            disabled={this.props.disabled && this.props.disabled}
            floated={this.props.floated && this.props.floated}
            fluid={this.props.fluid && this.props.fluid}
            hidden={this.props.hidden && this.props.hidden}
            href={this.props.href && this.props.href}
            inline={this.props.inline && this.props.inline}
            label={this.props.label && this.props.label}
            rounded={this.props.rounded && this.props.rounded}
            size={this.props.size && this.props.size}
            spaced={this.props.spaced && this.props.spaced}
            verticalAlign={this.props.verticalAlign && this.props.verticalAlign}
            wrapped={this.props.wrapped && this.props.wrapped}
          >
        </Image>
        </div>
      );
   }
  }

export default ImageLux