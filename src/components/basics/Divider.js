import React, { Component } from 'react';
import { Divider } from 'semantic-ui-react'
class DividerLux extends Component {
     render() {
      return (
        <div>
            <Divider
              className={this.props.className && this.props.className}
              clearing={this.props.clearing && this.props.clearing}
              content={this.props.content && this.props.content}
              fitted={this.props.fitted && this.props.fitted}
              hidden={this.props.hidden && this.props.hidden}
              horizontal={this.props.horizontal && this.props.horizontal}
              inverted={this.props.inverted && this.props.inverted}
              section={this.props.section && this.props.section}
              vertical={this.props.vertical && this.props.vertical}
            >
            </Divider>
        </div>
      );
   }
  }

export default DividerLux