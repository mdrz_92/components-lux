import React, { Component } from 'react';
import { Reveal } from 'semantic-ui-react'

class RevealLux extends Component {
     render() {
      return (
        <div>
          <Reveal 
            active={this.props.active && this.props.active}
            animated={this.props.animated && this.props.animated}
            children={this.props.children && this.props.children}
            className={this.props.className && this.props.className}
            content={this.props.content && this.props.content}
            disabled={this.props.disabled && this.props.disabled}
            instant={this.props.instant && this.props.instant}
          >
          {this.props.content && <Reveal.Content
            children={this.props.childrenContent && this.props.childrenContent}
            className={this.props.classNameContent && this.props.classNameContent}
            content={this.props.contentContent && this.props.contentContent}
            hidden={this.props.hiddenContent && this.props.hiddenContent}
            visible={this.props.visibleContent && this.props.visibleContent}
          >
          </Reveal.Content>}
        </Reveal>
        </div>
      );
   }
  }

export default RevealLux