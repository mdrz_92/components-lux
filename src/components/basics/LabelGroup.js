import React, { Component } from 'react';
import { Label } from 'semantic-ui-react'

class LabelGroupLux extends Component {
     render() {
      return (
        <div>
          <Label.Group
            circular={this.props.circular && this.props.circular}
            className={this.props.className && this.props.className}
            content={this.props.content && this.props.content}
            color={this.props.color && this.props.color}
            size={this.props.size && this.props.size}
            tag={this.props.tag && this.props.tag}
          >
            {this.props.labels && this.props.labels}
          </Label.Group>
        </div>
      );
   }
  }

export default LabelGroupLux