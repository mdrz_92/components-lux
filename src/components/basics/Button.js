import React, { Component } from 'react';
import { Button } from 'semantic-ui-react'

class ButtonLux extends Component {
     render() {
      return (
        <div>
          <Button 
            active={this.props.active ? this.props.active : false}
            animated={this.props.animated ? this.props.animated : false}
            attached={this.props.attached ? this.props.attached : false}
            basic={this.props.basic ? this.props.basic : false}
            circular={this.props.circular ? this.props.circular : false}
            className={this.props.className && this.props.className}
            color={this.props.color ? this.props.color : 'green'}
            compact={this.props.compact ? this.props.compact : false}
            disabled={this.props.disabled ? this.props.disabled : false}
            floated={this.props.floated && this.props.floated}
            fluid={this.props.fluid ? this.props.fluid : false}
            loading={this.props.loading ? this.props.loading : false}
            negative={this.props.negative ? this.props.negative : false}
            positive={this.props.positive ? this.props.positive : false}
            primary={this.props.primary ? this.props.primary : false}
            secondary={this.props.secondary ? this.props.secondary : false}
            toggle={this.props.toggle ? this.props.toggle : false}
            size={this.props.size ? this.props.size : 'medium'}
            onClick={this.props.onClick ? this.props.onClick : ()=>{console.log('click')}}
          >
          {this.props.icon && this.props.icon}
          {this.props.content && this.props.content}
        </Button>
        </div>
      );
   }
  }

export default ButtonLux