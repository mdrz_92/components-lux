import React, { Component } from 'react';
import { Segment } from 'semantic-ui-react'

class SegmentLux extends Component {
     render() {
      return (
        <div>
          <Segment 
            attached={this.props.attached && this.props.attached}
            basic={this.props.basic && this.props.basic}
            children={this.props.children && this.props.children}
            circular={this.props.circular && this.props.circular}
            className={this.props.className && this.props.className}
            clearing={this.props.clearing && this.props.clearing}
            color={this.props.color && this.props.color}
            compact={this.props.compact && this.props.compact}
            content={this.props.content && this.props.content}
            disabled={this.props.disabled && this.props.disabled}
            floated={this.props.floated && this.props.floated}
            inverted={this.props.inverted && this.props.inverted}
            loading={this.props.loading && this.props.loading}
            padded={this.props.padded && this.props.padded}
            piled={this.props.piled && this.props.piled}
            placeholder={this.props.placeholder && this.props.placeholder}
            raised={this.props.raised && this.props.raised}
            secondary={this.props.secondary && this.props.secondary}
            size={this.props.size && this.props.size}
            stacked={this.props.stacked && this.props.stacked}
            tertiary={this.props.tertiary && this.props.tertiary}
            textAlign={this.props.textAlign && this.props.textAlign}
            vertical={this.props.vertical && this.props.vertical}
          >
          {this.props.content && this.props.content}
          {this.props.inline && <Segment.Inline
            children={this.props.childrenInline && this.props.childrenInline}
            className={this.props.classNameInline && this.props.classNameInline}
            content={this.props.contentInline && this.props.contentInline}
          >
          </Segment.Inline>}
        </Segment>
        </div>
      );
   }
  }

export default SegmentLux