import React, { Component } from 'react';
import { Loader } from 'semantic-ui-react'
class LoaderLux extends Component {
     render() {
      return (
        <div>
            <Loader
              active={this.props.active && this.props.active}
              children={this.props.children && this.props.children}
              className={this.props.className && this.props.className}
              content={this.props.content && this.props.content}
              disabled={this.props.disabled && this.props.disabled}
              indeterminate={this.props.indeterminate && this.props.indeterminate}
              inline={this.props.inline && this.props.inline}
              inverted={this.props.inverted && this.props.inverted}
              size={this.props.size && this.props.size}
            >
            </Loader>
        </div>
      );
   }
  }

export default LoaderLux