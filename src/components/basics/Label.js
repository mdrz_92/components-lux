import React, { Component } from 'react';
import { Label } from 'semantic-ui-react'
class LabelLux extends Component {
     render() {
      return (
        <div>
            <Label
              active={this.props.active && this.props.active}
              attached={this.props.attached && this.props.attached}
              basic={this.props.basic && this.props.basic}
              circular={this.props.circular && this.props.circular}
              className={this.props.className && this.props.className}
              color={this.props.color && this.props.color}
              content={this.props.content && this.props.content}
              corner={this.props.corner && this.props.corner}
              detail={this.props.detail && this.props.detail}
              empty={this.props.empty && this.props.empty}
              floating={this.props.floating && this.props.floating}
              horizontal={this.props.horizontal && this.props.horizontal}
              icon={this.props.icon && this.props.icon}
              image={this.props.image && this.props.image}
              onClick={this.props.onClick && this.props.onClick}
              onRemove={this.props.onRemove && this.props.onRemove}
              pointing={this.props.pointing && this.props.pointing}
              removeIcon={this.props.removeIcon && this.props.removeIcon}
              ribbon={this.props.ribbon && this.props.ribbon}
              size={this.props.size && this.props.size}
              tag={this.props.tag && this.props.tag}
            >
                <Label.Detail
                    className={this.props.classNameDetail && this.props.classNameDetail}
                    content={this.props.contentDetail && this.props.contentDetail}
                >
                </Label.Detail>
            </Label>
        </div>
      );
   }
  }

export default LabelLux