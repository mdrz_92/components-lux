import React, { Component } from 'react';
import { Step } from 'semantic-ui-react'

class StepGroupLux extends Component {
     render() {
      return (
        <div>
          <Step.Group 
            attached={this.props.attached && this.props.attached}
            children={this.props.children && this.props.children}
            className={this.props.className && this.props.className}
            fluid={this.props.fluid && this.props.fluid}
            items={this.props.items && this.props.items}
            ordered={this.props.ordered && this.props.ordered}
            size={this.props.size && this.props.size}
            stackable={this.props.stackable && this.props.stackable}
            unstackable={this.props.unstackable && this.props.unstackable}
            vertical={this.props.vertical && this.props.vertical}
            widths={this.props.widths && this.props.widths}
          >
          {this.props.steps && this.props.steps}
        </Step.Group>
        </div>
      );
   }
  }

export default StepGroupLux