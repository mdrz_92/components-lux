import React, { Component } from 'react';
import { Container } from 'semantic-ui-react'
import HeaderLux from './HeaderLux';
class ContainerLux extends Component {
     render() {
      return (
        <div>
          <Container
            className={this.props.className && this.props.className}
            fluid={this.props.fluid ? this.props.fluid : false}
            text={this.props.text && this.props.text}
            textAlign={this.props.textAlign && this.props.textAlign}
            >
            <HeaderLux
                attached={this.props.attachedHeader ? this.props.attachedHeader : false}
                block={this.props.blockHeader ? this.props.blockHeader : false}
                className={this.props.classNameHeader && this.props.classNameHeader}
                color={this.props.colorHeader && this.props.colorHeader}
                content={this.props.contentHeader && this.props.contentHeader}
                size={this.props.sizeHeader && this.props.sizeHeader}
                subheader={this.props.subheaderHeader && this.props.subheaderHeader}
                textAlign={this.props.textAlignHeader && this.props.textAlignHeader}
                icon={this.props.iconHeader && this.props.iconHeader}
                iconContent={this.props.iconContentHeader && this.props.iconContentHeader}
            >
            </HeaderLux>
            {this.props.content && this.props.content}
            {this.props.custom && this.props.custom} 
          </Container>
        </div>
      );
   }
  }

export default ContainerLux