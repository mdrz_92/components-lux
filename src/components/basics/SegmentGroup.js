import React, { Component } from 'react';
import { Segment } from 'semantic-ui-react'

class SegmentGroupLux extends Component {
     render() {
      return (
        <div>
          <Segment.Group
            children={this.props.children && this.props.children}
            className={this.props.className && this.props.className}
            compact={this.props.compact && this.props.compact}
            content={this.props.content && this.props.content}
            horizontal={this.props.horizontal && this.props.horizontal}
            piled={this.props.piled && this.props.piled}
            raised={this.props.raised && this.props.raised}
            size={this.props.size && this.props.size}
            stacked={this.props.stacked && this.props.stacked}
          >
          {this.props.segments && this.props.segments}
        </Segment.Group>
        </div>
      );
   }
  }

export default SegmentGroupLux