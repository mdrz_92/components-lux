import React, { Component } from 'react';
import { List } from 'semantic-ui-react'

class ListLux extends Component {
     render() {
      return (
        <div>
          <List 
            animated={this.props.animated && this.props.animated}
            bulleted={this.props.bulleted && this.props.bulleted}
            celled={this.props.celled && this.props.celled}
            children={this.props.children && this.props.children}
            className={this.props.className && this.props.className}
            content={this.props.content && this.props.content}
            divided={this.props.divided && this.props.divided}
            floated={this.props.floated && this.props.floated}
            horizontal={this.props.horizontal && this.props.horizontal}
            inverted={this.props.inverted && this.props.inverted}
            items={this.props.items && this.props.items}
            link={this.props.link && this.props.link}
            onItemClick={this.props.onItemClick && this.props.onItemClick}
            ordered={this.props.ordered && this.props.ordered}
            relaxed={this.props.relaxed && this.props.relaxed}
            selection={this.props.selection && this.props.selection}
            size={this.props.size && this.props.size}
            verticalAlign={this.props.verticalAlign && this.props.verticalAlign}
          >
            {this.props.itemsList && this.props.itemsList}
        </List>
        </div>
      );
   }
  }

export default ListLux