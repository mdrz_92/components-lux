import React, { Component } from 'react';
import { Input } from 'semantic-ui-react'

class InputLux extends Component {
     render() {
      return (
        <div>
          <Input 
            action={this.props.action && this.props.action}
            actionPosition={this.props.actionPosition && this.props.actionPosition}
            className={this.props.className && this.props.className}
            disabled={this.props.disabled && this.props.disabled}
            error={this.props.error && this.props.error}
            fluid={this.props.fluid && this.props.fluid}
            focus={this.props.focus && this.props.focus}
            icon={this.props.icon && this.props.icon}
            iconPosition={this.props.icon && this.props.icon}
            input={this.props.input && this.props.input}
            inverted={this.props.inverted && this.props.inverted}
            label={this.props.label && this.props.label}
            labelPosition={this.props.labelPosition && this.props.labelPosition}
            loading={this.props.loading && this.props.loading}
            onChange={this.props.onChange && this.props.onChange}
            size={this.props.size && this.props.size}
            tabIndex={this.props.tabIndex && this.props.tabIndex}
            transparent={this.props.transparent && this.props.transparent}
            size={this.props.size && this.props.size}
          >
        </Input>
        </div>
      );
   }
  }

export default InputLux